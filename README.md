# The FedHax Project

<strong>An effort to catalogue various tweaks and scripts for Fedora 40</strong>

This is intended to be a live document which will be updated for upcoming releases.

Once the document is updated to reflect a new release of Fedora, compatibility with previous releases will no longer be guaranteed. The reader instead would need to find out whether the tweaks continue to work on their older releases.

<strong>Contents of the document will be exclusive to hacks personally tested on a system that is used daily as a primary device.</strong>
